'''
Created on 10 Mar 2014

@author: jmcd
'''
import creds
import boto.ec2.elb 
from boto.ec2.elb import HealthCheck


#connect to elb load balancer
elb = boto.ec2.elb.connect_to_region(creds.region,
                                     aws_access_key_id = creds.AWS_ACCESS_KEY, 
                                     aws_secret_access_key = creds.AWS_SECRET_KEY)
print 'Connected to:'
print elb

#create a health check
hc = HealthCheck(
        interval=20,
        healthy_threshold=3,
        unhealthy_threshold=5,
        target='HTTP:80/'
    )

#create load balancer
zones = creds.zones
ports = [(80, 80, 'http'), (443, 443, 'tcp')]
lb = elb.create_load_balancer(creds.lbname, zones, ports)

lbName = lb.configure_health_check(hc)

print 'load balancer created'


