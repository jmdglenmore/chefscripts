'''
Created on 15 Mar 2014

@author: jmcd
'''
import creds
import boto.ec2.autoscale
import boto.ec2.cloudwatch
from boto.ec2.autoscale import LaunchConfiguration
from boto.ec2.autoscale import AutoScalingGroup
from boto.ec2.autoscale import ScalingPolicy
from boto.ec2.cloudwatch import MetricAlarm

mylc = boto.ec2.autoscale.connect_to_region(creds.region,
                                                 aws_access_key_id = creds.AWS_ACCESS_KEY, 
                                                 aws_secret_access_key = creds.AWS_SECRET_KEY)
print 'Connected to'
print mylc
print 'Creating Launch Config'


lc = LaunchConfiguration(name=creds.lcname, image_id=creds.cusAmi,
                             key_name=creds.key,
                             security_groups=[creds.securityGroup])
mylc.create_launch_configuration(lc)
print 'Launch Config Created'
print lc.name


myag = boto.ec2.autoscale.connect_to_region(creds.region,
                                     aws_access_key_id = creds.AWS_ACCESS_KEY, 
                                     aws_secret_access_key = creds.AWS_SECRET_KEY)
print 'Creating Autoscaling Group'
ag = AutoScalingGroup(group_name=creds.asgname, load_balancers=[creds.lbname],
                          availability_zones=['eu-west-1a', 'eu-west-1b', 'eu-west-1c'],
                          launch_config=lc, min_size=1, max_size=3)
myag.create_auto_scaling_group(ag)

print 'Autoscaling Group Created'

jmcdUpPolicy = ScalingPolicy(name='A2JMcD-ScaleUp',
                                              adjustment_type='ChangeInCapacity',
                                              as_name=ag.name,
                                              scaling_adjustment=1,
                                              cooldown=180)
  
jmcdDownPolicy = ScalingPolicy(name='A2JMcD-ScaleDown',
                                              adjustment_type='ChangeInCapacity',
                                              as_name=ag.name,
                                              scaling_adjustment=-1,
                                              cooldown=180)  
myag.create_scaling_policy(jmcdUpPolicy)
myag.create_scaling_policy(jmcdDownPolicy)

alarm_dimensions = {"JmcdAsgName": creds.asgname}

cloudwatch = boto.ec2.cloudwatch.connect_to_region('eu-west-1')

scale_up_alarm = MetricAlarm(
            name='scale_up_on_cpu', namespace='AWS/EC2',
            metric='NetworkOut', statistic='Average',
            comparison='>', threshold='40000',
            period='60', evaluation_periods=2,
            alarm_actions=[jmcdUpPolicy.policy_arn],
            dimensions=alarm_dimensions)
cloudwatch.create_alarm(scale_up_alarm)
 
 
scale_down_alarm = MetricAlarm(
            name='scale_down_on_cpu', namespace='AWS/EC2',
            metric='NetworkIn', statistic='Average',
            comparison='<', threshold='40000',
            period='300', evaluation_periods=2,
            alarm_actions=[jmcdDownPolicy.policy_arn],
            dimensions=alarm_dimensions)
cloudwatch.create_alarm(scale_down_alarm)



